variable "environment" {
  type        = string
  description = "Environment name"
}

variable "name" {
  type        = string
  description = "General RDS name"
}

variable "username" {
  type        = string
  description = "Database username"
}

variable "password" {
  type        = string
  description = "Database password"
}

variable "port" {
  type        = number
  description = "Database port"
  default     = 3306
}

variable "subnet_ids" {
  type        = list(string)
  description = "Subnet identifiers"
}

variable "backup_retention_days" {
  type        = number
  description = "Number of days to save a backup"
  default     = 5
}

variable "skip_final_snapshot" {
  type        = bool
  description = "Skip or not the final DB snapshot"
  default     = false
}

variable "snapshot_identifier" {
  type        = string
  description = "Snapshot string identifier"
  default     = null
}
