# Aurora Module

This module is capable to generate a RDS Aurora Cluster instance with default needed configuration

Module Input Variables
----------------------

- `environment` - environment name
- `name` - general name
- `username` - database username
- `password` - database password
- `port` - database port
- `cluster_size` - database cluster size
- `subnet_ids` - subnets to locate database instance(s)
- `instance_class` - type of db instance (default db.t3.small)
- `logs` - cloudwatch logs (default ["audit", "error", "general", "slowquery"])
- `backup_retention_days` - number of days to retain backups (default 5)
- `skip_final_snapshot` - skip final snapshot (default false)
- `snapshot_identifier` - name on the final snapshot (required if skip_final_snapshot is false)

Usage
-----

```hcl
module "aurora" {
  source                = "git::https://gitlab.com/mesabg-tfmodules/aurora.git"

  environment           = "environment"
  name                  = "slugname"

  username              = "username"
  password              = "password"
  port                  = 3306
  cluster_size          = 2
  subnet_ids            = ["subnet-xxxxx", "subnet-aaaaa"]
  instance_class        = "db.t2.micro"
  logs                  = ["audit", "error", "general", "slowquery"]
  backup_retention_days = 5
  skip_final_snapshot   = true
}
```

Outputs
=======

 - `rds_cluster` - Created database cluster resource


Authors
=======
##### Moisés Berenguer <moises.berenguer@gmail.com>
