terraform {
  required_version = ">= 0.13.2"
}

resource "aws_db_subnet_group" "db_subnet_group" {
  name          = var.name
  subnet_ids    = var.subnet_ids

  tags = {
    Name        = var.name
    Environment = var.environment
  }
}

resource "aws_rds_cluster" "rds_cluster" {
  cluster_identifier              = var.name
  engine                          = "aurora-mysql"
  engine_version                  = "5.7.mysql_aurora.2.07.1"
  engine_mode                     = "serverless"
  database_name                   = "admin"
  master_username                 = var.username
  master_password                 = var.password
  port                            = var.port
  db_subnet_group_name            = aws_db_subnet_group.db_subnet_group.name
  apply_immediately               = true
  preferred_backup_window         = "00:00-01:00"
  backup_retention_period         = var.backup_retention_days
  skip_final_snapshot             = var.skip_final_snapshot
  snapshot_identifier             = var.snapshot_identifier

  scaling_configuration {
    auto_pause                    = false
    max_capacity                  = 256
    min_capacity                  = 1
    timeout_action                = "ForceApplyCapacityChange"
  }

  tags = {
    Name                          = var.name
    Environment                   = var.environment
  }
}
